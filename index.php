<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Add Record</h1>
                <label for="">Username</label>
                <input type="text" id="username" class="form-control" />
                <label for="">Password</label>
                <input type="text" id="password" class="form-control" />
                <label for="">Full Name</label>
                <input type="text" id="fullname" class="form-control" />
                <input type="button" value="Submit" class="btn btn-success mt-3" id="btn" />
            </div>
            <!-- col 6 end  -->
            <div class="col-md-6">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="mydata">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal" id="editmodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Edit User
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>Username</label>
                    <input type="text" id="usernameedit" class="form-control" />
                    <label>Fullname</label>
                    <input type="text" id="fullnameedit" class="form-control" />
                    <input type="hidden" id="editid" />
                    <button class="btn btn-success mt-3" onclick="updatedata()">Edit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="changemodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Change Password
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>New Password</label>
                    <input type="text" id="editpassword" class="form-control" />
                    
                    <input type="hidden" id="passwordid" />
                    <button class="btn btn-success mt-3" onclick="editPass()">Change</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('#btn').click(function(){
                let user = $('#username').val();
                let pass = $('#password').val();
                let fullname = $('#fullname').val();
                $.ajax({
                    url:'ajax.php',
                    type:'post',
                    data:{username:user,password:pass,fullname:fullname},
                    success:function(data){
                        alert(data);
                        getData();
                    }
                });
            });
        });
        function getData(){
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{getdata:''},
                success:function(data){
                    $('#mydata').html(data);
                }
            })
        }
        function deletedata(id){
            $.ajax({
                url:'ajax.php',
                type: 'post',
                data:{deleteid:id},
                success:function(data){
                    alert(data);
                    getData();
                }
            });
        }
        function editdata(id){
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{singleuserid:id},
                success: function(data){
                    // alert(JSON.stringify(data));
                    $('#editid').val(id);
                    $('#usernameedit').val(data.username);
                    $('#fullnameedit').val(data.name);
                }
            })
        }
        function updatedata(){
            let username = $('#usernameedit').val();
            let fullname = $('#fullnameedit').val();
            let id = $('#editid').val();
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{usernameedit:username,fullnameedit:fullname,editid:id},
                success:function(data){
                    getData();
                }
            });
        }
        function changedata(id){
            $('#passwordid').val(id);
        }
        function editPass(){
            let newpass = $('#editpassword').val();
            let id = $('#passwordid').val();
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{passwordedit:newpass,passid:id},
                success:function(data){
                    alert(data);
                    $('#changemodal').modal('toggle');
                }
            })
        }
        getData();
    </script>
</body>
</html>